@file:Suppress("unused")

package com.hadihariri.howtokotlin.dsl

data class Credential(var username: String = "", var password: String = "")
data class Configuration(var host: String = "", var port: Int = 0, var credentials: Credential = Credential())

fun main(args: Array<String>) {
    // with

    // apply
}
