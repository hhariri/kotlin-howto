package com.hadihariri.howtokotlin.expressions


fun divide(x: Int, y: Int): Int {
    try {
        return x / y
    } catch (e: ArithmeticException) {
        return -1
    }
}

