package com.hadihariri.howtokotlin.expressions

fun main(args: Array<String>) {

    val param = args[0]

    var value: Int

    if (param == "100") {
        value = 1
    } else if (param == "300") {
        value = 2
    } else {
        println("Something")
        value = 3
    }


    var anotherValue = 0

    when (value) {
        1 -> {
            println("It was one")
            anotherValue = 20
        }
        2 -> {
            println("it was two")
            anotherValue = 30
        }
        3, 4, 5 -> {
            println("One of these")
            anotherValue = 30
        }
        is Int -> {
            anotherValue = 20
            println("It's an Int")
        }
        in 0..100 -> {
            anotherValue = 40
            println("Other")
        }
        else -> {
            anotherValue = 0
        }
    }



    when {
        anotherValue == 0 -> println("It's 0")
        value == 10 -> println("It's 10")
    }

}
