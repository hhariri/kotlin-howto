@file:Suppress("USELESS_ELVIS")

package com.hadihariri.howtokotlin.expressions

import com.hadihariri.howtokotlin.stdlib.*


fun processPerson(person: Person) {
    val name = person.name
    if (name == null)
        throw IllegalArgumentException(
                "Named required")
    val age = person.age
    if (age == null) return

    println("$name: $age")
}
