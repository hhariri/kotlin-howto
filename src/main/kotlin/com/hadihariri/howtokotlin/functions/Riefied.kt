@file:Suppress("unused")

package com.hadihariri.howtokotlin.functions

import java.util.*

fun <T> loadService(clazz: Class<T>) =
        ServiceLoader.load(clazz)





inline fun <reified T: Any> loadServiceAlt() =
        ServiceLoader.load(T::class.java)