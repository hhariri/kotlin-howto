@file:Suppress("unused")

package com.hadihariri.howtokotlin.types


class MyTest {
    class State(val data: String)

    var state: State? = null

    fun initCode() {
        state = State("abc")
    }

    fun useCode() {
        println(state!!.data.length)
    }
}
