@file:Suppress("unused")

package com.hadihariri.howtokotlin.types


class MyStringClass {
    fun convertToCamelCase() {

    }

    fun convertToLowerCamelCase() {

    }

    fun convertToSpecialInvertedQuotesCase() {

    }
}

// Move to extension functions

// Keep surface area small
