@file:Suppress("unused")

package com.hadihariri.howtokotlin.stdlib

data class Person(val name: String, val age: Int)

fun sortPersons(persons: List<Person>) =
        persons.sortedWith(
                Comparator { p1, p2 ->
                    val rc = p1.name.compareTo(p2.name)
                    if (rc != 0) {
                        rc
                    } else {
                        p1.age - p2.age
                    }
                })


fun sortPersonsAlt(persons: List<Person>) =
        persons.sortedWith(compareBy(Person::name, Person::age))
