package com.hadihariri.howtokotlin.stdlib

fun updateProgress(value: Int) {
    val newValue = when {
        value < 0   -> 0
        value > 100 -> 100
        else        -> value
    }
}

fun updateProgressAlt(value: Int) {
    var newValue = value.coerceIn(0, 100)
}


