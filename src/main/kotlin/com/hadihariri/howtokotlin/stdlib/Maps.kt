@file:Suppress("unused", "UNUSED_PARAMETER")

package com.hadihariri.howtokotlin.stdlib

class Entry(val error: String?)

fun listErrors(results: List<Entry>) =
        results.map {
            it.error
        }.filterNotNull()
