package com.hadihariri.howtokotlin.delegation

import kotlin.properties.*

var watchingObservable by Delegates.observable("value") { _, old, new ->
    println("Everything $old is again $new")
}

fun main(args: Array<String>) {
    watchingObservable = "Hello"
}