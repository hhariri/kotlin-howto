package com.hadihariri.howtokotlin.delegation

private var _systemInfo: String? = null
val systemInfo: String
    get() {
        if (_systemInfo == null) {
            println("Enquiring")
            _systemInfo = "${System.getProperty("os.name")} - ${System.getProperty("os.version")}"
        }
        return _systemInfo!!
    }

fun main(args: Array<String>) {
    for (i in 1..3) {
        println(systemInfo)
    }
}