package com.hadihariri.howtokotlin.delegation

var watching = "value"
    set(new) {
        val old = field
        field = new
        println("Everything $old is again $new")
    }


fun main(args: Array<String>) {
    watching = "Hello"
}

