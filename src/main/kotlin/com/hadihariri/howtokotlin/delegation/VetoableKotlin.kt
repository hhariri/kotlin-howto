package com.hadihariri.howtokotlin.delegation

import kotlin.properties.*


var valueVetoable: String by Delegates.vetoable("nothing") { _, _, new ->
    println("Changing value to $new as condition is met")
    new.startsWith("v")
}

fun main(args: Array<String>) {
    value = "something"
    value = "valid"
}