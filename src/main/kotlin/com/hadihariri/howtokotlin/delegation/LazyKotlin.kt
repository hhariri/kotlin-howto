package com.hadihariri.howtokotlin.delegation


val systemInfoLazy: String by lazy {
    println("Enquiring")
    "${System.getProperty("os.name")} - ${System.getProperty("os.version")}"
}

fun main(args: Array<String>) {
    for (i in 1..3) {
        println(systemInfoLazy)
    }
}