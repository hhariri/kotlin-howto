package com.hadihariri.howtokotlin.delegation


var value: String = "nothing"
    set(new) {
        if (new.startsWith("v")) {
            println("Changing the value to $new as condition is met")
            field = new
        }
    }

fun main(args: Array<String>) {
    value = "something"
    value = "valid"
}